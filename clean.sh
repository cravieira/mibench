#!/bin/bash

EXE_SUFFIX=.x

for i in $(ls -d */*/Makefile); do
    path=$(dirname $i)
    make clean -C $path EXE_SUFFIX=$EXE_SUFFIX
done

# Remove dumps
rm -rf dump
