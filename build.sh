#!/bin/bash

set -e

# Default toolchain
#GCC=gcc
#DUMP=objdump
#STRIP=strip

# ARM toolchain
GCC=arm-linux-gnueabihf-gcc
DUMP=arm-linux-gnueabihf-objdump
STRIP=arm-linux-gnueabihf-strip

EXE_SUFFIX=.x

OUTDIR=$PWD/bin
bench_sets="*/*/runme_small*.txt"

# Build
for i in $(ls -d $bench_sets); do
    path=$(dirname $i)
    make -C $path CC=$GCC STRIP=$STRIP EXE_SUFFIX=$EXE_SUFFIX
done
# Create dumps
mkdir -p dump
for bin in $(find ./ -type f -name "*$EXE_SUFFIX"); do
    path=$(dirname $bin)
    bench=$(basename -s .x $bin)

    $DUMP -D $bin > dump/$bench.dump
done
